from django.db import models
from retaguarda.models import G3ModelClientes


class Pessoa(G3ModelClientes):
    PESSOA_JURIDICA = "PJ"
    PESSOA_FISICA = "PF"

    tipo_pessoa = [
        (PESSOA_FISICA, "Pessoa Física"),
        (PESSOA_JURIDICA, "Pessoa Jurídica"),
    ]

    nome = models.CharField(max_length=300)
    tipo = models.CharField(max_length=2, choices=tipo_pessoa)
    documento = models.CharField(max_length=9, null=True, blank=True)

    def __str__(self):
        return self.nome
