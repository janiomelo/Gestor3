from django.db import models
from Gestor3.middleware import RequestMiddleware
from django.contrib.auth.models import User
from django.utils import timezone
from sqlalchemy.ext.hybrid import hybrid_property


class G3Model(models.Model):
    data_inclusao = models.DateTimeField(auto_now_add=True)
    usuario_inclusao = models.IntegerField(null=True, blank=True)
    data_alteracao = models.DateTimeField(auto_now=True)
    usuario_alteracao = models.IntegerField(null=True, blank=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        request = RequestMiddleware(get_response=None)
        if hasattr(request.thread_local, 'current_request'):
            request = request.thread_local.current_request
            usuario_logado = request.user.id

            if self.pk is None:
                self.usuario_inclusao = usuario_logado
            self.usuario_alteracao = usuario_logado
        super(G3Model, self).save(*args, **kwargs)


class G3Cliente(G3Model):
    PESSOA_JURIDICA = "PJ"
    PESSOA_FISICA = "PF"

    tipo_pessoa = [
        (PESSOA_FISICA, "Pessoa Física"),
        (PESSOA_JURIDICA, "Pessoa Jurídica"),
    ]

    nome = models.CharField(max_length=300)
    tipo = models.CharField(max_length=2, choices=tipo_pessoa)
    documento = models.CharField(max_length=9, null=True, blank=True)

    def __str__(self):
        return self.nome


class G3Usuario(User):
    cliente = models.ForeignKey(
        G3Cliente, on_delete=models.PROTECT, related_name='usuarios_cliente')


class G3Oferta(G3Model):
    nome = models.CharField(max_length=100)
    valor = models.DecimalField(max_digits=13, decimal_places=2)

    def __str__(self):
        return self.nome


class G3Assinatura(G3Model):
    ASSINATURA_VINDI = "assinatura_vindi"

    SITUACAO_ATIVA = 'ativa'
    SITUACAO_INATIVA = 'inativa'

    formas_de_pagamento = [
        (ASSINATURA_VINDI, "Assinatura da VINDI")
    ]
    cliente = models.ForeignKey(
        G3Cliente, on_delete=models.CASCADE,
        related_name="assinaturas_do_cliente")
    oferta = models.ForeignKey(G3Oferta, on_delete=models.PROTECT)
    dia_vencimento = models.IntegerField()
    forma_pagamento = models.CharField(
        max_length=30, choices=formas_de_pagamento)
    inicio = models.DateTimeField(auto_now_add=True)
    fim = models.DateTimeField(null=True, blank=True)

    @hybrid_property
    def situacao(self):
        if (self.fim is None or
                self.fim.timestamp() > timezone.now().timestamp()):
            return G3Assinatura.SITUACAO_ATIVA
        return G3Assinatura.SITUACAO_INATIVA

    def __str__(self):
        return "{0} - {1}".format(self.cliente, self.oferta)


class G3ModelClientes(G3Model):
    g3_cliente = models.ForeignKey(G3Cliente, on_delete=models.PROTECT)
    class Meta:
        abstract = True
