from django.contrib import admin
from .models import G3Oferta, G3Cliente, G3Assinatura, G3Usuario


admin.site.register(G3Assinatura)
admin.site.register(G3Cliente)
admin.site.register(G3Oferta)
admin.site.register(G3Usuario)
