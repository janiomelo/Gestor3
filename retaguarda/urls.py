from django.urls import include, path
from rest_framework_nested import routers
from .views import (
    G3ClienteViewSet, G3AssinaturaViewSet, G3OfertaViewSet, G3UsuarioViewSet
)

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'clientes', G3ClienteViewSet, base_name='g3clientes')
router.register(r'ofertas', G3OfertaViewSet, base_name='g3ofertas')
router.register(r'assinaturas', G3AssinaturaViewSet, base_name='g3assinaturas')
router.register(r'usuarios', G3UsuarioViewSet, base_name='g3usuarios')

clientes_router = routers.NestedSimpleRouter(router, r'clientes', lookup='cliente')
clientes_router.register(r'assinaturas', G3AssinaturaViewSet, base_name='cliente-assinaturas')


urlpatterns = [
    path('', include(router.urls)),
    path('', include(clientes_router.urls)),
]