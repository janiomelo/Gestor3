import factory
import random
from faker import Faker
from faker.providers import BaseProvider
from . import models


class Provider(BaseProvider):
    def nf_fatura_outros(self):
        return str(random.randint(1, 9999))

    def cheque(self):
        return str(random.randint(1,999999)).zfill(6)

    def decimal(self):
        return random.randint(0,999999) / 100

    def cpf(self):
        return '333222111'


def random_object(list_objects: list):
    i = random.randint(0, len(list_objects) - 1)
    return list_objects[i]


fake = Faker('pt_BR')
fake.add_provider(Provider)


class G3ClienteFactory(factory.django.DjangoModelFactory):
    nome = fake.name()
    tipo = random_object(['PF', 'PJ'])
    documento = fake.cpf()

    class Meta:
        model = models.G3Cliente


class G3OfertaFactory(factory.django.DjangoModelFactory):
    nome = fake.name()
    valor = fake.decimal()

    class Meta:
        model = models.G3Oferta


class G3AssinaturaFactory(factory.django.DjangoModelFactory):
    cliente = factory.SubFactory(G3ClienteFactory)
    oferta = factory.SubFactory(G3OfertaFactory)
    dia_vencimento = fake.day_of_month()
    forma_pagamento = models.G3Assinatura.ASSINATURA_VINDI
    inicio = fake.date_object()
    fim = None

    class Meta:
        model = models.G3Assinatura


class G3UsuarioFactory(factory.django.DjangoModelFactory):
    cliente = factory.SubFactory(G3ClienteFactory)
    username = fake.first_name()
    first_name = fake.first_name()
    last_name = fake.last_name()
    email = fake.email()
    is_active = True
    password = 'password'

    class Meta:
        model = models.G3Usuario