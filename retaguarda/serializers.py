from rest_framework import serializers
from .models import G3Oferta, G3Cliente, G3Assinatura, G3Usuario
from django.utils import timezone
from django.db.models import Q


class G3ModelSerializer(serializers.ModelSerializer):
    class Meta:
        abstract = True
        read_only_fields = (
            'data_inclusao',
            'usuario_inclusao',
            'data_alteracao',
            'usuario_alteracao')


class G3OfertaSerializer(G3ModelSerializer):
    class Meta:
        model = G3Oferta
        fields = ['id', 'nome', 'valor']


class G3ClienteSerializerDepth(G3ModelSerializer):
    assinaturas_ativas = serializers.SerializerMethodField()

    class Meta:
        model = G3Cliente
        fields = ['id', 'nome', 'tipo', 'documento', 'assinaturas_ativas']
        depth = 2

    def get_assinaturas_ativas(self, obj):
        a = G3Assinatura.objects.filter(cliente=obj.id).filter(
            Q(fim__gte=timezone.now()) | Q(fim__isnull=True)
        )
        return G3AssinaturaSerializerDepth(a, many=True).data


class G3ClienteSerializer(G3ModelSerializer):
    class Meta:
        model = G3Cliente
        fields = ['id', 'nome', 'tipo', 'documento']


class G3AssinaturaSerializerDepth(G3ModelSerializer):
    cliente = G3ClienteSerializer()
    oferta = G3OfertaSerializer()

    situacao = serializers.SerializerMethodField()

    def get_situacao(self, obj):
        return obj.situacao

    class Meta:
        model = G3Assinatura
        fields = [
            'id', 'cliente', 'oferta', 'dia_vencimento', 'forma_pagamento',
            'inicio', 'fim', 'situacao'
        ]
        depth = 1


class G3AssinaturaSerializer(G3ModelSerializer):
    situacao = serializers.SerializerMethodField()

    def get_situacao(self, obj):
        return obj.situacao

    class Meta:
        model = G3Assinatura
        fields = [
            'id', 'cliente', 'oferta', 'dia_vencimento', 'forma_pagamento',
            'inicio', 'fim', 'situacao'
        ]


class G3UsuarioSerializer(G3ModelSerializer):
    class Meta:
        model = G3Usuario
        fields = [
            'id', 'cliente', 'username', 'first_name', 'last_name', 'email'
        ]
        write_only = [
            'password'
        ]
        read_only = [
            'is_active', 'last_login', 'date_joined'
        ]


class G3UsuarioEditSerializer(G3ModelSerializer):
    class Meta:
        model = G3Usuario
        fields = [
            'id', 'first_name', 'last_name', 'email'
        ]
        write_only = [
            'password'
        ]
        read_only = [
            'is_active', 'last_login', 'date_joined'
        ]
