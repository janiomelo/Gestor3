from ..models import G3Cliente
from django.urls import reverse
from rest_framework import status
from .helpers import MyAPITestCase
from ..factories import G3ClienteFactory, G3AssinaturaFactory
from django.utils import timezone


class G3ClienteTests(MyAPITestCase):

    def test_consigo_criar(self):
        dto = {
            "nome": "José da Silva",
            "documento": "111222333",
            "tipo": "PF",
        }

        response = self.client.post(
            reverse('g3clientes-list'), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            G3Cliente.objects.filter(documento='111222333').count(), 1)

    def test_consigo_alterar(self):
        obj = G3ClienteFactory(documento="888999333")
        dto = {
            "nome": "José da Silva",
            "documento": "111222333",
            "tipo": "PF",
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3clientes-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            G3Cliente.objects.filter(documento='111222333').count(), 1)

    def test_consigo_filtrar_por_tipo(self):
        G3ClienteFactory(tipo=G3Cliente.PESSOA_JURIDICA)
        G3ClienteFactory(tipo=G3Cliente.PESSOA_JURIDICA)
        G3ClienteFactory(tipo=G3Cliente.PESSOA_JURIDICA)
        G3ClienteFactory(tipo=G3Cliente.PESSOA_FISICA)
        G3ClienteFactory(tipo=G3Cliente.PESSOA_FISICA)

        response = self.client.get(reverse('g3clientes-list') + "?tipo=PF")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        response = self.client.get(reverse('g3clientes-list') + "?tipo=PJ")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        response = self.client.get(reverse('g3clientes-list') + "?tipo=pf")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_consigo_filtrar_por_documento(self):
        G3ClienteFactory(documento='112233')
        G3ClienteFactory()
        G3ClienteFactory()
        G3ClienteFactory()

        response = self.client.get(reverse('g3clientes-list') + "?documento=112233")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_consigo_buscar(self):
        G3ClienteFactory(nome="Pedro Manuel", documento='112233')
        G3ClienteFactory(nome="Maria Pereira", documento='122233')
        G3ClienteFactory(nome='Jose da Silva', documento='132233')
        G3ClienteFactory(nome="Igor da Silva", documento='102233')

        response = self.client.get(reverse('g3clientes-list') + "?search=112")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.get(reverse('g3clientes-list') + "?search=josé")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.get(reverse('g3clientes-list') + "?search=ilv")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_consigo_deletar(self):
        obj = G3ClienteFactory()
        kwargs = {"pk": obj.id}
        response = self.client.delete(
            reverse('g3clientes-detail', kwargs=kwargs))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            G3Cliente.objects.filter(pk=obj.id).count(), 0)


    def test_consigo_ver_assinaturas_ativas(self):
        obj = G3ClienteFactory()
        G3AssinaturaFactory(cliente=obj)
        G3AssinaturaFactory(cliente=obj, fim=timezone.now())

        kwargs = {"pk": obj.id}
        response = self.client.get(reverse('g3clientes-detail', kwargs=kwargs))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['assinaturas_ativas']), 1)
