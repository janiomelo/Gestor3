from ..models import G3Usuario, G3Cliente
from django.urls import reverse
from rest_framework import status
from .helpers import MyAPITestCase
from ..factories import G3UsuarioFactory, G3ClienteFactory


class G3UsuarioTests(MyAPITestCase):

    def test_consigo_criar(self):
        cliente = G3ClienteFactory()
        dto = {
            'cliente': {'id': cliente.id},
            'username': 'maria1',
            'first_name': 'Maria',
            'last_name': 'Silva',
            'email': 'maria.silva@email.com',
            'password': 'password'
        }

        response = self.client.post(
            reverse('g3usuarios-list'), dto, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(G3Usuario.objects.count(), 1)

    def test_consigo_alterar(self):
        obj = G3UsuarioFactory(
            first_name='José', last_name='Oliveira',
            email='jose.oliveira@email.com')
        dto = {
            "first_name": "José Pedro",
            "last_name": "Oliveira Brito",
            "email": "jose.brito@email.com"
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3usuarios-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        obj_novo = G3Usuario.objects.get(pk=obj.id)
        self.assertEqual(obj_novo.first_name, "José Pedro")
        self.assertEqual(obj_novo.last_name, "Oliveira Brito")
        self.assertEqual(obj_novo.email, "jose.brito@email.com")


    def test_nao_consigo_alterar_cliente(self):
        cliente_novo = G3ClienteFactory()
        obj = G3UsuarioFactory()
        dto = {
            "cliente": {"id": cliente_novo.id}
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3usuarios-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            G3Usuario.objects.get(pk=obj.id).cliente.id, obj.cliente.id)

    def test_nao_consigo_alterar_username(self):
        obj = G3UsuarioFactory(username='username1')
        dto = {
            "username": "username2"
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3usuarios-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            G3Usuario.objects.get(pk=obj.id).username, "username1")

    def test_consigo_buscar(self):
        pass

    def test_consigo_deletar(self):
        obj = G3UsuarioFactory()
        kwargs = {"pk": obj.id}
        response = self.client.delete(
            reverse('g3usuarios-detail', kwargs=kwargs))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            G3Usuario.objects.filter(pk=obj.id).count(), 0)
