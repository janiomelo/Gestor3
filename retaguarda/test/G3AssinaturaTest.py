from ..models import G3Assinatura
from ..factories import G3AssinaturaFactory, G3OfertaFactory, G3ClienteFactory
from ..serializers import G3OfertaSerializer, G3ClienteSerializer
from django.urls import reverse
from rest_framework import status
from .helpers import MyAPITestCase
from django.utils import timezone


class G3AssinaturaTests(MyAPITestCase):

    def test_consigo_criar(self):
        cliente = G3ClienteFactory()
        oferta = G3OfertaFactory()
        dto = {
            "cliente": {"id": cliente.id},
            "oferta": {"id": oferta.id},
            "dia_vencimento": 10,
            "forma_pagamento": G3Assinatura.ASSINATURA_VINDI,
            "inicio": timezone.now(),
            "fim": None,
        }

        response = self.client.post(
            reverse('g3assinaturas-list'), dto, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(G3Assinatura.objects.count(), 1)

    def test_consigo_alterar(self):
        G3AssinaturaFactory()
        G3AssinaturaFactory()
        obj = G3AssinaturaFactory()
        oferta = G3OfertaFactory()
        dto = {
            "oferta": G3OfertaSerializer(oferta).data,
            "dia_vencimento": 10,
            "forma_pagamento": G3Assinatura.ASSINATURA_VINDI,
            "inicio": timezone.now(),
            "fim": None,
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3assinaturas-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            G3Assinatura.objects.filter(oferta=oferta.id).count(), 1)

    def test_nao_consigo_alterar_cliente(self):
        obj = G3AssinaturaFactory()
        cliente = G3ClienteFactory()
        dto = {
            "oferta": G3OfertaSerializer(obj.oferta).data,
            "cliente": G3ClienteSerializer(cliente).data,
            "dia_vencimento": 10,
            "forma_pagamento": G3Assinatura.ASSINATURA_VINDI,
            "inicio": timezone.now(),
            "fim": None,
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3assinaturas-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            G3Assinatura.objects.get(pk=obj.id).cliente.id, obj.cliente.id)

    def test_consigo_verificar_situacao_ativa(self):
        obj = G3AssinaturaFactory()
        kwargs = {"pk": obj.id}
        response = self.client.get(
            reverse('g3assinaturas-detail', kwargs=kwargs))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data['situacao'], G3Assinatura.SITUACAO_ATIVA)

    def test_consigo_verificar_situacao_inativa(self):
        obj = G3AssinaturaFactory(fim=timezone.now())
        kwargs = {"pk": obj.id}
        response = self.client.get(
            reverse('g3assinaturas-detail', kwargs=kwargs))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data['situacao'], G3Assinatura.SITUACAO_INATIVA)

    def test_consigo_filtrar_por_cliente(self):
        cliente = G3ClienteFactory()
        G3AssinaturaFactory(cliente=cliente)
        G3AssinaturaFactory()
        G3AssinaturaFactory()
        G3AssinaturaFactory()

        response = self.client.get(
            reverse('g3assinaturas-list') + "?cliente={0}".format(cliente.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_consigo_filtrar_por_oferta(self):
        oferta = G3OfertaFactory()
        G3AssinaturaFactory(oferta=oferta)
        G3AssinaturaFactory()
        G3AssinaturaFactory()
        G3AssinaturaFactory()

        response = self.client.get(
            reverse('g3assinaturas-list') + "?oferta={0}".format(oferta.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_consigo_deletar(self):
        obj = G3AssinaturaFactory()
        kwargs = {"pk": obj.id}
        response = self.client.delete(
            reverse('g3assinaturas-detail', kwargs=kwargs))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            G3Assinatura.objects.filter(pk=obj.id).count(), 0)
