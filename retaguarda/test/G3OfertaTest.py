from ..models import G3Oferta
from django.urls import reverse
from rest_framework import status
from .helpers import MyAPITestCase
from ..factories import G3OfertaFactory


class G3OfertaTests(MyAPITestCase):

    def test_consigo_criar(self):
        dto = {
            "nome": "Plano Mensal",
            "valor": 150.25,
        }

        response = self.client.post(
            reverse('g3ofertas-list'), dto, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(G3Oferta.objects.count(), 1)

    def test_consigo_alterar(self):
        obj = G3OfertaFactory(nome="Plano Mensal", valor=100.12)
        dto = {
            "nome": "Plano Anual",
            "valor": 150.21,
        }
        kwargs = {"pk": obj.id}
        response = self.client.put(
            reverse('g3ofertas-detail', kwargs=kwargs), dto, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            G3Oferta.objects.filter(nome="Plano Anual").count(), 1)

    def test_consigo_buscar(self):
        G3OfertaFactory(nome="Plano ABC")
        G3OfertaFactory(nome="Plano Mensal")
        G3OfertaFactory(nome="Plano Anual")
        G3OfertaFactory(nome="Plano CBA")

        response = self.client.get(
            reverse('g3ofertas-list') + "?search=ensa")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.get(
            reverse('g3ofertas-list') + "?search=abc")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.get(
            reverse('g3ofertas-list') + "?search=ANUAL")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_consigo_deletar(self):
        obj = G3OfertaFactory()
        kwargs = {"pk": obj.id}
        response = self.client.delete(
            reverse('g3ofertas-detail', kwargs=kwargs))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            G3Oferta.objects.filter(pk=obj.id).count(), 0)
