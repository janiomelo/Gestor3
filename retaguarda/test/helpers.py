from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


class MyAPITestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.auth()

    def auth(self):
        user = User.objects.create_user('usuario', 'usuario@email.com', 'senha')
        token = Token.objects.create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
