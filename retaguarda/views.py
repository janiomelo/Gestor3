from rest_framework import viewsets
from django.shortcuts import get_object_or_404
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from .models import G3Assinatura, G3Cliente, G3Oferta, G3Usuario
from .serializers import (
    G3AssinaturaSerializerDepth, G3UsuarioSerializer, G3OfertaSerializer,
    G3ClienteSerializerDepth, G3AssinaturaSerializer, G3UsuarioEditSerializer
)
from django_filters.rest_framework import FilterSet
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.response import Response


class MySearchFilter(SearchFilter):

    def get_search_terms(self, request):
        params = request.query_params.get(self.search_param, '')
        terms = params.replace(',', ' ').split()

        import unidecode
        terms = [unidecode.unidecode(term) for term in terms]
        return terms


class G3OfertaViewSet(viewsets.ModelViewSet):
    queryset = G3Oferta.objects.all().order_by('nome')
    serializer_class = G3OfertaSerializer
    filter_backends = (MySearchFilter,)
    search_fields = ('nome',)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)


class G3ClienteFilter(FilterSet):
    class Meta:
        model = G3Cliente
        fields = ['tipo', 'nome', 'documento']


class G3ClienteViewSet(viewsets.ModelViewSet):
    queryset = G3Cliente.objects.all().order_by('nome')
    serializer_class = G3ClienteSerializerDepth
    filter_class = G3ClienteFilter
    filter_backends = (MySearchFilter, DjangoFilterBackend, OrderingFilter)
    search_fields = ('nome', 'documento')
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)


class G3AssinaturaFilter(FilterSet):
    class Meta:
        model = G3Assinatura
        fields = ['cliente', 'oferta']


class G3AssinaturaViewSet(viewsets.ModelViewSet):
    queryset = G3Assinatura.objects.all().order_by('inicio')
    serializer_class = G3AssinaturaSerializerDepth
    filter_class = G3AssinaturaFilter
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        cliente = request.data.pop('cliente')
        oferta = request.data.pop('oferta')
        request.data['cliente'] = cliente['id']
        request.data['oferta'] = oferta['id']

        serializer = G3AssinaturaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        if 'cliente' in request.data.keys():
            request.data.pop('cliente')

        oferta = request.data.pop('oferta')
        request.data['oferta'] = oferta['id']
        request.data['cliente'] = instance.cliente.id

        serializer = G3AssinaturaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK,
                        headers=headers)


class G3ViewSetClientes(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        g3_user = get_object_or_404(G3Usuario, pk=self.request.user.id)
        return super().get_queryset().filter(g3_cliente_id=g3_user.cliente_id)


class G3UsuarioViewSet(viewsets.ModelViewSet):
    serializer_class = G3UsuarioSerializer
    queryset = G3Usuario.objects.all().order_by('first_name')

    def create(self, request, *args, **kwargs):
        cliente = request.data.pop('cliente')
        request.data['cliente'] = cliente['id']

        serializer = G3UsuarioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        if 'cliente' in request.data.keys():
            request.data.pop('cliente')

        serializer = G3UsuarioEditSerializer(
            data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK,
                        headers=headers)
