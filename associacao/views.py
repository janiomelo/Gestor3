from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from django_filters.rest_framework import FilterSet
from .models import Associado
from .serializers import AssociadoSerializer
from retaguarda.views import G3ViewSetClientes


class AssociadoFilter(FilterSet):
    class Meta:
        model = Associado
        fields = ['tipo', 'nome', 'documento', 'matricula']


class AssociadoViewSet(G3ViewSetClientes):
    queryset = Associado.objects.all().order_by('nome')
    serializer_class = AssociadoSerializer
    filter_class = AssociadoFilter
    filter_backends = (SearchFilter, DjangoFilterBackend, OrderingFilter)
    search_fields = ('nome', 'matricula', 'documento')
    ordering_fields = ('id', 'tipo', 'nome', 'documento', 'matricula')
