from django.urls import include, path
from rest_framework import routers
from .views import AssociadoViewSet

router = routers.DefaultRouter()
router.register(r'associados', AssociadoViewSet)


urlpatterns = [
    path('', include(router.urls)),
]