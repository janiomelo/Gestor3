from django.db import models
from cadastro.models import Pessoa


class Associado(Pessoa):
    matricula = models.PositiveIntegerField()
