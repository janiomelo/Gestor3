from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.authentication import TokenAuthentication

urlpatterns = [
    path('admin/', admin.site.urls),
    path('g3/', include('retaguarda.urls')),
    path('', include('cadastro.urls')),
    path('', include('associacao.urls')),
    path(
        'api-auth/', include(
            'rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth')
]
