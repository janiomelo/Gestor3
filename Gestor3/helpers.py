from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from django_filters.rest_framework import DjangoFilterBackend
import json


class CustomFilterBackend(DjangoFilterBackend):
    def get_filterset_kwargs(self, request, queryset, view):
        qp_dict = request.query_params.dict()
        python_dict = qp_dict
        if 'filter' in qp_dict.keys():
            python_dict = json.loads(qp_dict['filter'])
        return {
            'data': python_dict,
            'queryset': queryset,
            'request': request,
        }


class ContentRangeHeaderPagination(PageNumberPagination):

    def get_paginated_response(self, data):
        total_items = self.page.paginator.count
        item_starting_index = self.page.start_index() - 1 # In a page, indexing starts from 1
        item_ending_index = self.page.end_index() - 1

        content_range = 'items {0}-{1}/{2}'.format(item_starting_index, item_ending_index, total_items)

        headers = {
            'Content-Range': content_range,
            'X-Total-Count':self.page.paginator.count
        }
        # return Response(OrderedDict([
        #            ('count', self.page.paginator.count),
        #            ('next', self.get_next_link()),
        #            ('previous', self.get_previous_link()),
        #            ('results', data)
        #        ]), headers=headers)
        return Response(data, headers=headers)
